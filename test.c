#include <stdio.h>    // Standard input/output definitions
#include <stdlib.h>
#include <string.h>   // String function definitions
#include <unistd.h>   // para usleep()
#include <getopt.h>
#include <math.h>

#include "arduino-serial-lib.h"

struct Nodelist
{
 int temp;
 int hum;
 struct Nodelist* next;
};

float calculateSD(float data[]);

void error(char* msg)
{
    fprintf(stderr, "%s\n",msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
	int fd = -1;
	int temp = -1;
	int hum = -1;
	int cont = 1;
	struct nodelist *nuevo;
	
	int baudrate = 9600;  // default

	fd = serialport_init("/dev/ttyACM0", baudrate);

	if( fd==-1 )
	{
		error("couldn't open port");
		return -1;
	}else
	{
		while(cont!=0)
		{
			temp = serialport_write(fd,"t");
			hum = serialport_write(fd,"h");
			/*nuevo->temp = temp;
			nuevo->hum = hum;
			nuevo->next = NULL;*/
			sleep(5);
		}
	}

	
	close( fd );
	return 0;	
}

/* Ejemplo para calcular desviacion estandar y media */
float calculateSD(float data[])
{
    float sum = 0.0, mean, standardDeviation = 0.0;

    int i;

    for(i = 0; i < 10; ++i)
    {
        sum += data[i];
    }

    mean = sum/10;

    for(i = 0; i < 10; ++i)
        standardDeviation += pow(data[i] - mean, 2);

    return sqrt(standardDeviation / 10);
}

